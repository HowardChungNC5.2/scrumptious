from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm

# Create your views here.
def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": recipe,
    }
    return render(request, "recipes/detail-final.html", context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }

    return render(request, "recipes/list-final.html", context)

def create_recipe(request):
    if request.method == "POST":
        # use the form to validate the values
            # save values to the database
        form = RecipeForm(request.POST)
        if form.is_valid():
            form.save()
            # redirect browser to list of recipes page
            return redirect("recipe_list")
    else:
        # create instance of Django model form class
        form = RecipeForm()

        context = {
            "form": form,
        }
        return render(request, "recipes/create.html", context)

def edit_recipe(request, id):
    # get object to be edited using Django function
    # Post: model class object from db
    # id: object property key
    recipe = get_object_or_404(Recipe, id=id)
    # if HTTP POST
    if request.method == "POST":
        # validate and save updated data with Django model form
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    # otherwise
    else:
        # create a form with object's data
        form = RecipeForm(instance=recipe)
        context = {
            'recipe': recipe,
            'form': form,
        }
        # show form with object's data in HTML
        return render(request, 'recipes/edit.html', context)

def delete_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        recipe.delete()
        return redirect('recipe_list')
    context = {
        'delete_recipe': recipe
    }
    return render(request, 'recipes/delete.html', context)

# from django.contrib.auth.decorator import login_required
# @login_required
# def create_recipe(request).....
