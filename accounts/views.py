from django.shortcuts import render, redirect
from django.contrib.auth import login
from django.contrib.auth.models import User
from accounts.forms import SignUpForm

# Create your views here.
def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']

            if password == password_confirmation:
                # create a new user with those values
                # save it to a var
                user = User.objects.create_user(
                    # USERNAME='USERNAME'
                    username,
                    password=password,
                    first_name=first_name,
                    last_name=last_name,
                )
                # login the user with the user you just
                # created
                login(request, user, backend=None)
                return redirect('recipe_list')
            else:
                form.add_error("password", "Passwords do not match")

    else:
        form = SignUpForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)
